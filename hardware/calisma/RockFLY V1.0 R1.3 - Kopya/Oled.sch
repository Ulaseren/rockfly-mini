EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 15
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L roket-rescue:oled-oled J3
U 1 1 60987007
P 4400 3450
F 0 "J3" H 4400 3817 50  0000 C CNN
F 1 "oled" H 4400 3726 50  0000 C CNN
F 2 "others:oled" H 4400 3450 50  0001 L BNN
F 3 "" H 4400 3450 50  0001 L BNN
F 4 "4" H 4400 3450 50  0001 L BNN "Number_of_Positions"
F 5 "2.54 mm[.1 in]" H 4400 3450 50  0001 L BNN "Centerline_Pitch"
F 6 "103185-4" H 4400 3450 50  0001 L BNN "Comment"
F 7 "Not Compliant" H 4400 3450 50  0001 L BNN "EU_RoHS_Compliance"
F 8 "Connector" H 4400 3450 50  0001 L BNN "Product_Type"
	1    4400 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR042
U 1 1 609873AB
P 3800 3500
F 0 "#PWR042" H 3800 3250 50  0001 C CNN
F 1 "GND" H 3805 3327 50  0000 C CNN
F 2 "" H 3800 3500 50  0001 C CNN
F 3 "" H 3800 3500 50  0001 C CNN
	1    3800 3500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR041
U 1 1 609878BC
P 3800 3300
F 0 "#PWR041" H 3800 3150 50  0001 C CNN
F 1 "+3.3V" H 3815 3473 50  0000 C CNN
F 2 "" H 3800 3300 50  0001 C CNN
F 3 "" H 3800 3300 50  0001 C CNN
	1    3800 3300
	1    0    0    -1  
$EndComp
Text HLabel 5050 3350 2    50   Input ~ 0
SCL_OLED
Text HLabel 5050 3450 2    50   Input ~ 0
SDA_OLED
Wire Wire Line
	3800 3300 3800 3350
Wire Wire Line
	3800 3350 3900 3350
Wire Wire Line
	3900 3450 3800 3450
Wire Wire Line
	3800 3450 3800 3500
Wire Wire Line
	4900 3350 5050 3350
Wire Wire Line
	4900 3450 5050 3450
$EndSCHEMATC
